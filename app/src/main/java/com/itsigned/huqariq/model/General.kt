package com.itsigned.huqariq.model

import org.json.JSONArray
import org.json.JSONObject

data class getCountries(
        var countries: JSONObject
)

data class sendState(
        var states_id:Int,
        var countries_id:Int
)

data class sendCountry(
        var countries_id:Int
)

data class LoginRequestDto(
        var email:String,
        var password:String
)

data class LoginRequestDtoOnlyEmail(
        var email:String
)

data class RecuperateEmail(
        var email:String
)

data class SetNewPassEmail(
        var email:String,
        var code:String,
        var password:String
)

data class LoginUserDto(
        var count: Any,
        var user_id: Int,
        var name: String,
        /*var last_name: String,*/
        /*var dni: Int,*/
        var lang:Int,
        var langs: Any,
        var email: String
)

data class RegisterUserDto(
        var email: String,
        var password:String,
        /*var phone:String,*/
        var states_id:String,
        var cities_id:String,
        /*var last_name:String,*/
        var name:String,
        var countries_id:String,
        /*var dni:String,*/
        var native_lang:Int
)

data class RequestValidateMail(
        var email:String
)

data class RequestValidateDni(
        var dni:String
)

data class  ResponseLangDto(
        val language:List<Language>
)

data class  ResponseValidateDni(
        val name:String,
        val first_name: String,
        val last_name: String
)

data class Language(
        val iso:String,
        val language_id:Int,
        val name:String
){
    override fun toString(): String {
        return name
    }
}


data class FormRegisterUserStepOneDto(
        var email:String,
        var password:String,
        /*var dni:String,*/
        var name:String
        /*var surname:String*/
)

data class FormRegisterUserStepTwoDto(
        var countriesId :String,
        var statesId :String,
        var citiesId :String,
        /*var phone:String,*/
        var nativeLang:Int
)


data class FormRegisterStepThreeDto(
       var idDialect:String
)

data class FormDialectRegion(
        var departamento:String,
        var provincia:String,
        var distrito:String
)

data class FormDialectAnswer(
        var respuesta:Array<String>

)


data class ResponseDialectRegion(
        var dialecto:String
)

data class ResponseDialectAnswer(
        var dialecto:String
)