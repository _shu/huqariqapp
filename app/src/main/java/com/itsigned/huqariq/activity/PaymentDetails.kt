package com.itsigned.huqariq.activity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.itsigned.huqariq.R
import kotlinx.android.synthetic.main.paypal_payment_details.*
import org.json.JSONException
import org.json.JSONObject

class PaymentDetails ():AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paypal_payment_details)

        //get intent

        val intent:Intent=getIntent()
        try {
            val jsonObject:JSONObject= JSONObject(intent.getStringExtra("PaymentDetails"))
            showDetails(jsonObject.getJSONObject("response"),intent.getStringExtra("PaymentAmount"))
        } catch (e:JSONException){
            e.printStackTrace()
        }

    }

    fun showDetails(response:JSONObject,paymentAmount:String){
        try {
            txtId.text=response.getString("id")
            txtStatus.text=response.getString("state")
            txtStatus.text=("$"+paymentAmount)
        } catch (e:JSONException) {
            e.printStackTrace()
        }
    }

}