package com.itsigned.huqariq.activity

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.itsigned.huqariq.R
import com.itsigned.huqariq.helper.PermissionHelper
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.android1n.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

private const val REQUEST_SIGNUP = 0
class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.android1n)
        if (SessionManager.getInstance(this).isLogged) goToActivity()
        configureActionButton()
        backGroundColor()
        PermissionHelper.recordAudioPermmision(this, null)
        closeGoogleSession()
        /*printHashKey(this)*/
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_android_1)
    }

    /**
     * Metodo con las configuraciones iniciales de los botones
     */
    private fun configureActionButton() {
        btnRegistrese.setOnClickListener {
            val intent = Intent(this, RegisterActivityTour::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
        Text_Ingrese_Aqui.setOnClickListener {
            val intent = Intent(this, LoginActivityTour::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
    }

    private fun closeGoogleSession() {
        val googleConf: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        val googleClient: GoogleSignInClient = GoogleSignIn.getClient(this,googleConf)
        googleClient.signOut()
    }

    private fun printHashKey(context: Context){

        try {
            val info = packageManager.getPackageInfo(
                "com.itsigned.huqariq",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: NoSuchAlgorithmException) {
        }

        /*try{
            var info:PackageInfo=context.packageManager.getPackageInfo(context.packageName,PackageManager.GET_SIGNATURES)
            for (signature:Signature in info.signatures) {
                val md:MessageDigest= MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey:String= Base64.encode(md.digest(),0).toString()
                Log.i("hashKey","printHashKey() Hash Key:" + hashKey)
            }
        } catch (e:NoSuchAlgorithmException) {
            Log.e("error","printHashKey()",e)
        } catch (e:Exception) {
            Log.e("error","printHashKey()",e)
        }*/
    }
}