package com.itsigned.huqariq.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.fragment.*
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.RecuperateEmail
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_tour.*
import kotlinx.android.synthetic.main.activity_main_tour.bottomNavigation
import kotlinx.android.synthetic.main.android4.*

class MainActivityTour: AppCompatActivity(),MainActivityTourInterface {

    private var idCurrentFragment: Int = -1

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tour)
        changeBackgroundColor(R.color.main_status_bar,R.color.white)
        defaultFragment()
        var fragment: Fragment?
        bottomNavigation.setOnNavigationItemSelectedListener { item ->

            fragment = when (item.itemId) {
                idCurrentFragment -> null
                R.id.home_option -> {
                    item.isChecked = true
                    idCurrentFragment = R.id.home_option
                    MainStartFragment()

                }

                R.id.settings_option -> {
                    item.isChecked = true
                    idCurrentFragment = R.id.config_option
                    MainConfigFragment()
                }

                R.id.about_option->{
                    item.isChecked = true
                    idCurrentFragment = R.id.about_option
                    MainAboutFragment()
                }
                else -> null
            }

            transitionFragment(fragment)
        }
    }

    /**
     * Metodo para setear el fragmento por defecto
     */
    private fun defaultFragment() {
        val starterFragment = MainStartFragment()
        transitionFragment(starterFragment)

        bottomNavigation.selectedItemId = 0
    }

    /**
     * Metodo para transicionar el fragmento dado
     * @param fragment el fragmento a transicionar
     * @return un boleano indicando el exito de la transacción
     */
    private fun transitionFragment(fragment: Fragment?): Boolean {
        if (fragment == null) return false
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layoutMainTour, fragment, "Fragment")
        transaction.commit()
        return true
    }

    override fun changeFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layoutMainTour, fragment, "Fragment")
        transaction.addToBackStack("default")
        transaction.commit()
    }

    override fun uncheck(int: Int,bool:Boolean) {
        when(int){
            1->bottomNavigation.menu.getItem(0).isChecked=bool
            2->bottomNavigation.menu.getItem(1).isChecked=bool
            3->bottomNavigation.menu.getItem(2).isChecked=bool
        }
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    override fun changeBackgroundColor(color:Int, bckgrnd:Int) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, color)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(bckgrnd)
    }

    override fun paypalPayment() {
        val intent = Intent(this, PayPalActivityTour::class.java)
        startActivity(intent)
    }



}

interface MainActivityTourInterface{
    fun paypalPayment()
    fun changeBackgroundColor(color:Int, bckgrnd:Int)
    fun changeFragment(fragment: Fragment)
    fun uncheck(int: Int,bool:Boolean)

}