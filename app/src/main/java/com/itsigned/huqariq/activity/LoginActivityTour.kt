package com.itsigned.huqariq.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.bean.User
import com.itsigned.huqariq.fragment.*
import com.itsigned.huqariq.helper.getLoadingProgress
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.helper.hasErrorEditTextEmpty
import com.itsigned.huqariq.helper.showError
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.*
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.android27.*
import kotlinx.android.synthetic.main.stepper_header.*
import java.util.*


class LoginActivityTour : AppCompatActivity(), LoginActivityTourInterface {

    private var currentFragment: Fragment? = null

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.android3)
        backGroundColor()
        configureActionButton()
        /*customProgressDialog=getLoadingProgress()
        if(intent.extras!=null&&intent.extras!!.containsKey("email")){
            mapValues["email"]=intent.getStringExtra("email")
        }
        transaction()
        buttonNext.setOnClickListener { (currentFragment as StepFragment).verifyStep() }*/

    }*/

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_tour)
        if (SessionManager.getInstance(this).isLogged) goToActivity()
        transaction()
        backGroundColor()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

    /**
     * Metodo con las configuraciones iniciales de los botones
     */
    private fun configureActionButton() {
        btnLogin.setOnClickListener {
            login()
        }
        tv_forget_password.setOnClickListener {
            loadFragment(UsageConditionsFragment())
        }
    }

    private fun transaction() {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        currentFragment = LogInFragment()
        ft.add(R.id.layoutLoginTour, currentFragment!!)
        ft.commit()
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layoutLoginTour, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    /**
     * Metodo para validar los datos del login
     */
    override fun login() {
        if (hasErrorEditTextEmpty(etEmail, R.string.registro_message_error_ingrese_correo_electronico)) return
        if (hasErrorEditTextEmpty(etPass, R.string.registro_message_error_ingrese_contrasena_usuario)) return
        if (!Util.validarCorreo(etEmail.text.toString())) {
            showError(etEmail, getString(R.string.registro_message_error_correo_electronico_invalido))
            return
        }
        verifyLoginExtern()
    }

    override fun login_google_facebook(mail:String){
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.loginApp(this,
                LoginRequestDtoOnlyEmail(email = mail),
                { loginUser ->
                    progress.dismiss()
                    SessionManager.getInstance(baseContext).createUserSession(GeneralMapper.loginUserDtoDtoToUser(loginUser,0))
                    println(loginUser)
                    goToActivity()
                },
                { error ->
                    progress.dismiss()
                    if(error==getString(R.string.error_login)){
                        Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                    }
                })
    }

    /**
     * Metodo para autentificar al usuario
     */
    private fun verifyLoginExtern() {
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.loginUser(this,
                LoginRequestDto(email = etEmail.text.toString(), password = etPass.text.toString()),
                { loginUser ->
                    progress.dismiss()
                    println(loginUser)
                    SessionManager.getInstance(baseContext).createUserSession(GeneralMapper.loginUserDtoDtoToUser(loginUser,0))
                    goToActivity()
                },
                { error ->
                    progress.dismiss()
                    Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                })
    }

    override fun changePass(mail:String) {
        /*val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()*/
        println(mail)
        RafiServiceWrapper.sendChangeCode(this,
            RecuperateEmail(mail),
            { /*progress.dismiss()*/
                /*SessionManager.getInstance(baseContext).createUserSession(GeneralMapper.loginUserDtoDtoToUser(loginUser))
                println(loginUser)
                goToActivity()*/
            },
            { error ->
                Toast.makeText(this, error, Toast.LENGTH_LONG).show()
                /*progress.dismiss()*/
                /*if(error==getString(R.string.error_login)){
                    Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                }*/
            })
    }

    override fun setNewPass(mail: String, code: String, pass: String) {
        RafiServiceWrapper.setNewPass(this,
            SetNewPassEmail(mail,etCodigo.text.toString(),etPassword.text.toString()),
            {
                this.supportFragmentManager!!.popBackStack(1, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            },
            { error ->
                Toast.makeText(this, error, Toast.LENGTH_LONG).show()
            })
    }
}

interface LoginActivityTourInterface {
    fun login() {}
    fun login_google_facebook(mail:String) {}
    fun changePass(mail:String)
    fun setNewPass(mail:String,code:String,pass:String)
}