package com.itsigned.huqariq.activity

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.bean.User
import com.itsigned.huqariq.fragment.*
import com.itsigned.huqariq.helper.getLoadingProgress
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.*
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.android3.*
import kotlinx.android.synthetic.main.android27.*
import kotlinx.android.synthetic.main.android7_10stepper.*
import kotlinx.android.synthetic.main.stepper_header.*
import java.util.*


class RegisterActivityTour : AppCompatActivity(), RegisterActivityTourInterface {

    private var formRegisterUserStepOneDto:FormRegisterUserStepOneDto?=null
    private var formRegisterUserStepTwoDto:FormRegisterUserStepTwoDto?=null
    private var formRegisterUserStepThreeDto:FormRegisterStepThreeDto?=null

    private var listAnswer= arrayOf<String>("Respuesta 1","Respuesta 2","Respuesta 3","Respuesta 4","Respuesta 5")
    lateinit var customProgressDialog: Dialog
    private var idDialec=-1
    private var actual:Int=0

    private var toThisFragment:Fragment?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_tour)
        defaultFragment()
        changeBackgroundColor(R.drawable.degradado_status_bar)
    }

    private fun defaultFragment(){
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.add(R.id.layoutRegisterTour, RegisterFragment(),"defaultFragment")
        ft.commit()
    }

    override fun customFragment(wichFragment:Fragment,withName:String,wichStack:String){
        toThisFragment=wichFragment
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.layoutRegisterTour, toThisFragment!!,withName)
        ft.addToBackStack(wichStack)
        ft.commit()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    override fun changeBackgroundColor(int: Int) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(int)
    }

    override fun setDataFormSteperOne(form: FormRegisterUserStepOneDto) { formRegisterUserStepOneDto=form }

    override fun setDataFormSteperTwo(form: FormRegisterUserStepTwoDto) {formRegisterUserStepTwoDto=form}

    override fun setDataFormSteperThree(form: FormRegisterStepThreeDto) {formRegisterUserStepThreeDto=form}

    override fun registerNewUser() {
        val usuario = User()

        usuario.name = formRegisterUserStepOneDto!!.name
        /*usuario.lastName = ""*/
        usuario.email = formRegisterUserStepOneDto!!.email
        usuario.password = formRegisterUserStepOneDto!!.password
        /*usuario.phone = ""
        usuario.dni = (9999999..100000000).random().toString()*/
        usuario.codePais=formRegisterUserStepTwoDto!!.countriesId.toInt()
        usuario.codeEstado=formRegisterUserStepTwoDto!!.citiesId.toInt()
        usuario.codeCiudad=formRegisterUserStepTwoDto!!.statesId.toInt()
        /*usuario.codePais=15
        usuario.codeCiudad=1
        usuario.codeEstado=1*/
        /*usuario.avance=0*/
        /*usuario.lengActual=0*/
        usuario.idLanguage=formRegisterUserStepThreeDto!!.idDialect.toInt()
        Log.d("user for register",usuario.toString())
        registerByServiceWeb(usuario)
    }

    /**
     * Metodo que invoca el webService para registrar el usuario
     * @param user objeto del tipo Usuario con la información del usuario
     */
    private fun registerByServiceWeb(user:User){
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.registerUser(this, GeneralMapper.userToRegisterUserDto(user),{
            progress.dismiss()
            verifyLoginExtern(user)
        },{x->
            progress.dismiss()
            Toast.makeText(baseContext, x, Toast.LENGTH_LONG).show()
        })
    }

    /**
     * Metodo para autenticar un usuario
     * @param user datos del usuario a autentificar
     */
    private fun verifyLoginExtern(user: User) {
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.loginUser(this,
                LoginRequestDto(email = user.email, password = user.password),
                { loginUser ->
                    progress.dismiss()
                    createSession(GeneralMapper.loginUserDtoDtoToUser(loginUser,0)) },
                { error ->
                    progress.dismiss()
                    Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                })
    }

    /**
     * Metodo para crear una sesión del usuario y llevarlo a la vista principal
     */
    private fun createSession(user: User){
        SessionManager.getInstance(baseContext).createUserSession(user)
        user.userExternId = 0
        setResult(Activity.RESULT_OK, null)
        finish()
        goToActivity()
    }

    override fun verifyDialect() {
        customProgressDialog=getLoadingProgress()
        customProgressDialog.show()
        RafiServiceWrapper.validateDialectAnswer(this, FormDialectAnswer(listAnswer),
                {x->
                    idDialec = if (x.dialecto.toUpperCase(Locale.ROOT)=="CHANCA") 1
                    else ( if(x.dialecto.toUpperCase(Locale.ROOT) == "COLLAO") 2 else -1)
                    customProgressDialog.dismiss()
                    val form=getForm()
                    setDataFormSteperThree(form)
                    if(idDialec==-1){
                        // val a=Toast.makeText(context!!,R.string.message_error_question,Toast.LENGTH_LONG)
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(R.string.message_error_question)
                                .setPositiveButton(R.string.dialog_acept,
                                        DialogInterface.OnClickListener { dialog, id ->
                                            dialog.dismiss()
                                            this.finish()
                                        })
                        // Create the AlertDialog object and return it
                        val d= builder.create()
                        d.setCancelable(false)
                        d.show()
                    } else {
                        customFragment(VariationResultFragment(),"ResultVariationFragment","default")
                    }
                },{
            Toast.makeText(this,R.string.default_error_server, Toast.LENGTH_LONG).show()
            customProgressDialog.dismiss()
        })
    }

    private fun getForm(): FormRegisterStepThreeDto {
        return FormRegisterStepThreeDto(idDialec.toString())
    }

    override fun addResponse(nro:Int,rpta:String) {
        listAnswer[nro]=rpta
    }

    override fun sayActual(update: Boolean):Int{
        if(update)
            {actual++
            return actual
        }
        else
            {return actual
        }
    }

    override fun sayDialect():Int {
        return idDialec
    }

    override fun resetCoutner() {
        actual=0
    }

    override fun fragmentIndexed(index: Int,type:Int):Fragment{
        var f:Fragment?=null
        if(type==0){
            f=IntroOneFragment()
        } else {
            f=VariationTwoFragment()
        }
        val args=Bundle()
        args.putInt("index",index)
        f.setArguments(args)
        return f!!
    }
}



interface RegisterActivityTourInterface{
    fun setDataFormSteperOne(form:FormRegisterUserStepOneDto)
    fun setDataFormSteperTwo(form:FormRegisterUserStepTwoDto)
    fun setDataFormSteperThree(form:FormRegisterStepThreeDto)
    fun registerNewUser()
    fun customFragment(wichFragment:Fragment,withName:String,wichStack:String)
    fun verifyDialect()
    fun addResponse(nro:Int,rpta:String)
    fun sayDialect():Int
    fun sayActual(update:Boolean):Int
    fun resetCoutner()
    fun fragmentIndexed(index:Int,type:Int):Fragment
    fun changeBackgroundColor(int: Int)
}