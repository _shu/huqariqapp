package com.itsigned.huqariq.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.dialog.PlayAudioDialog
import com.itsigned.huqariq.fragment.IntroOneFragment
import com.itsigned.huqariq.fragment.VariationOneFragment
import com.itsigned.huqariq.fragment.VariationResultFragment
import com.itsigned.huqariq.fragment.VariationThreeFragment
import com.itsigned.huqariq.helper.PermissionHelper
import com.itsigned.huqariq.helper.getLoadingProgress
import com.itsigned.huqariq.model.FormDialectAnswer
import com.itsigned.huqariq.model.FormRegisterStepThreeDto
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import kotlinx.android.synthetic.main.android14.*
import java.util.*

class VariationActivityTour : AppCompatActivity(){

    private  var fragmentDynamic: Fragment?=null
    private var actual:Int=0
    private var listAnswer= arrayOf<String>("Respuesta 1","Respuesta 2","Respuesta 3","Respuesta 4","Respuesta 5","Respuesta 6")
    lateinit var customProgressDialog: Dialog
    private var idDialec=-1

    /**
     * Metodo para la creación de activitys
     * @param savedInstanceState Bundle con información de la actividad previa
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_variation_tour)
        backGroundColor()
        supportFragmentManager.beginTransaction().add(R.id.layoutVariationTour, VariationOneFragment()).commit()
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

    private fun changeBackground(estado: Boolean) {
        if (estado) window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
        else window.setBackgroundDrawableResource(R.drawable.degradado_status_bar)
    }

    private fun changeFragment() {
        if (actual<5){
            fragmentDynamic=VariationThreeFragment()
            val at: FragmentTransaction = supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutVariationTour, fragmentDynamic!!,"Question"+actual)
            at.addToBackStack(null)
            at.commit()
            actual=+1
        } else {
            fragmentDynamic=VariationResultFragment()
            val at: FragmentTransaction = supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutVariationTour, fragmentDynamic!!,"Question"+actual)
            at.addToBackStack(null)
            at.commit()
        }
    }

    private fun proceedNextActivity(){
        val intent = Intent(this, RegisterActivityTour::class.java)
        startActivityForResult(intent, 0)
    }


    private fun actualQuestion(): Int {
        return actual-1
    }

    private fun verifyDialect() {
        customProgressDialog=getLoadingProgress()
        customProgressDialog.show()
        RafiServiceWrapper.validateDialectAnswer(this, FormDialectAnswer(listAnswer),
                {x->
                    idDialec = if (x.dialecto.toUpperCase(Locale.ROOT)=="CHANCA") 1
                    else ( if(x.dialecto.toUpperCase(Locale.ROOT) == "COLLAO") 2 else -1)
                    customProgressDialog.dismiss()
                    val form=getForm()
                    if(idDialec==-1){
                        // val a=Toast.makeText(context!!,R.string.message_error_question,Toast.LENGTH_LONG)
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(R.string.message_error_question)
                                .setPositiveButton(R.string.dialog_acept,
                                        DialogInterface.OnClickListener { dialog, id ->
                                            dialog.dismiss()
                                            this.finish()
                                        })
                        // Create the AlertDialog object and return it
                        val d= builder.create()
                        d.setCancelable(false)
                        d.show()
                    } else {
                        proceedNextActivity()
                    }
                },{
            Toast.makeText(this,R.string.default_error_server, Toast.LENGTH_LONG).show()
            customProgressDialog.dismiss()
        })
    }

    private fun getForm(): FormRegisterStepThreeDto {
        return FormRegisterStepThreeDto(idDialec.toString())
    }

}