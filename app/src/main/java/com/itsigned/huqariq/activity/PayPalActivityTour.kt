package com.itsigned.huqariq.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itsigned.huqariq.R
import com.itsigned.huqariq.payments.PayPalConfig
import com.paypal.android.sdk.payments.*
import kotlinx.android.synthetic.main.paypal_amount.*
import org.json.JSONException

class PayPalActivityTour(): AppCompatActivity() {

    var paypalConfig:PayPalConfig= PayPalConfig()
    var PAYPAL_REQUESTO_CODE:Int=7171
    
    private val config:PayPalConfiguration=PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(paypalConfig.PAYPAL_CLIENT_ID)

    var amount:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paypal_amount)

        //Start paypal service
        val intent = Intent(this,PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config)
        startService(intent)

        payButton.setOnClickListener(){
            processPayment()
        }
    }

    fun processPayment(){
        amount=amountInputEditText.text.toString()
        val paypalPayment= PayPalPayment(amount.toString().toBigDecimal(),"USD","Donate for Huqariq",PayPalPayment.PAYMENT_INTENT_SALE)
        val intent = Intent(this,PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,paypalPayment)
        startActivityForResult(intent,PAYPAL_REQUESTO_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==PAYPAL_REQUESTO_CODE){
            if (resultCode== Activity.RESULT_OK){
                var confirmation:PaymentConfirmation=data!!.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
                if(confirmation!=null){
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString()
                        startActivity(Intent(this,PaymentDetails::class.java).putExtra("PaymentDetails",paymentDetails).putExtra("PaymentAmount",amount))
                    }catch (e: JSONException){
                        e.printStackTrace()
                    }
                }
            } else if (resultCode==Activity.RESULT_CANCELED) {
                Toast.makeText(this,"Canel",Toast.LENGTH_SHORT).show()
            }
        } else if (resultCode==PaymentActivity.RESULT_EXTRAS_INVALID) {
            Toast.makeText(this,"Invalid",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        stopService(Intent(this,PayPalService::class.java))
        super.onDestroy()
    }

}