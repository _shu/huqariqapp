package com.itsigned.huqariq.activity

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.AssetManager
import android.os.Bundle
import android.os.Environment
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.itsigned.huqariq.R
import com.itsigned.huqariq.database.DataBaseService
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.util.session.SessionManager
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        backGroundColor()
        /*read_json()*/
        /*downloadtxt()*/
        AppCenter.start(application, "9080c742-2242-4b86-8e4f-c971edeb4158",
                Analytics::class.java, Crashes::class.java)

        val task: TimerTask = object : TimerTask() {
            override fun run() {
                val isLogued=SessionManager.getInstance(this@SplashActivity).isLogged
                if (isLogued) goToActivity() else goToActivity(StartActivity::class.java)
            }
        }
        val timer = Timer()
        timer.schedule(task, 4000)
    }

    /*Metodo para hacer la barra de estado (iconos superiores) y
    barra de navegacion (iconos inferiores) transparentes*/
    fun backGroundColor() {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
        /*window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)*/
        window.setBackgroundDrawableResource(R.drawable.degradado_android_1)
    }

    fun Context.readJsonAsset(fileName:String):String{
        val inputStream=assets.open(fileName)
        val size=inputStream.available()
        val buffer=ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        return String(buffer,Charsets.UTF_8)
    }

    fun printJson(){
        for (i in 0 .. readJsonAsset("countries_states_cities.json").length)
        println()
    }

    fun downloadtxt(){
        var fOut:FileOutputStream?=null
        val directory:File=File(Environment.getExternalStorageDirectory(),"cities")
        if(!directory.exists()){
            directory.mkdirs()
        }
        try {
            fOut= FileOutputStream(File(directory,"cities.txt"))
        } catch (e:FileNotFoundException) {
            e.printStackTrace()
        }
        var osw:OutputStreamWriter = OutputStreamWriter(fOut)
        try {
            var cadena:String?=null
            var pais:String?=null
            var json:String?=null
            var a:Boolean=false
            val inputStream:InputStream=assets.open("countries_states_cities.json")
            json=inputStream.bufferedReader().use { it.readText() }
            var jsonarr=JSONArray(json)
            for (i in 0..jsonarr.length()-1){
                a=false
                var jsonobj=jsonarr.getJSONObject(i)
                if(jsonobj.optJSONObject("translations")!=null){
                    if(jsonobj.optJSONObject("translations").optString("es")!=null){
                        a=true
                        pais=jsonobj.optJSONObject("translations").optString("es").replace("[\'\"]","")
                        cadena="("+(i+1)+", 0, 0,'"+pais+"'),"
                        osw.write(cadena)
                        /*println("("+(i+1)+", 0, 0, '"+jsonobj.optJSONObject("translations").optString("es")+"'),")*/
                        /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 0, 0,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")*/
                    }
                } else {
                    pais=jsonobj.getString("name").replace("[\'\"]","")
                    cadena="("+(i+1)+", 0, 0,'"+pais+"'),"
                    osw.write(cadena)
                    /*println("("+(i+1)+", 0, 0, '"+jsonobj.getString("name")+"),")
                    db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 0, 0,"+'"'+jsonobj.getString("name")+'"'+")")*/
                }
                if(jsonobj.optJSONArray("states")!=null&&jsonobj.optJSONArray("states").length()>0){
                    var jsonarr2=jsonobj.getJSONArray("states")
                    for(j in 0..jsonarr2.length()-1){
                        var jsonobj2=jsonarr2.getJSONObject(j)
                        pais=jsonobj2.getString("name").replace("[\'\"]","")
                        cadena="("+(i+1)+", "+(j+1)+", 0,'"+pais+"'),"
                        osw.write(cadena)
                        /*println("("+(i+1)+", "+(j+1)+", 0, '"+jsonobj2.getString("name")+"')")
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", 0,"+'"'+jsonobj2.getString("name")+'"'+")")*/
                        if(jsonobj2.optJSONArray("cities")!=null&&jsonobj2.optJSONArray("cities").length()>0){
                            var jsonarr3=jsonobj2.getJSONArray("cities")
                            for(k in 0..jsonarr3.length()-1){
                                var jsonobj3=jsonarr3.getJSONObject(k)
                                /*println("("+(i+1)+", "+(j+1)+", "+(k+1)+", '"+jsonobj3.getString("name")+"')")*/
                                /*if(i==183&&j==34&&k==216){*/
                                    pais=jsonobj3.getString("name").replace("[\'\"]","")
                                    cadena="("+(i+1)+", "+(j+1)+", "+(k+1)+", '"+pais+"'),"
                                    osw.write(cadena)
                                    /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", "+(k+1)+", '"+jsonobj3.getString("name")+"')")*/
                                /*} else {
                                    pais=jsonobj3.getString("name").replace("[\'\"]","")
                                    cadena="("+(i+1)+", "+(j+1)+", "+(k+1)+", "+'"'+pais+'"'+"),"
                                    osw.write(cadena)
                                    /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", "+(k+1)+", "+'"'+jsonobj3.getString("name")+'"'+")")*/
                                }*/

                            }
                        } else {
                            pais=jsonobj2.getString("name").replace("[\'\"]","")
                            cadena="("+(i+1)+", "+(j+1)+", 1,'"+pais+"'),"
                            osw.write(cadena)
                            /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", 1,"+'"'+jsonobj2.getString("name")+'"'+")")*/
                        }
                    }
                } else {
                    if (a==true) {
                        pais=jsonobj.optJSONObject("translations").optString("es").replace("[\'\"]","")
                        cadena="("+(i+1)+", 1, 0,'"+pais+"'),"
                        osw.write(cadena)
                        /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 0,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")*/
                        pais=jsonobj.optJSONObject("translations").optString("es").replace("[\'\"]","")
                        cadena="("+(i+1)+", 1, 1,'"+pais+"'),"
                        osw.write(cadena)
                        /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 1,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")*/
                    } else {
                        pais=jsonobj.getString("name").replace("[\'\"]","")
                        cadena="("+(i+1)+", 1, 0,'"+pais+"'),"
                        osw.write(cadena)
                        /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 0,"+'"'+jsonobj.getString("name")+'"'+")")*/
                        pais=jsonobj.getString("name").replace("[\'\"]","")
                        cadena="("+(i+1)+", 1, 1,'"+pais+"'),"
                        osw.write(cadena)
                        /*db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 1,"+'"'+jsonobj.getString("name")+'"'+")")*/
                    }
                }
            }
            osw.flush()
            osw.close()
        } catch (f:IOException) {
            f.printStackTrace()
        }
    }

    /*fun read_json(){
        val databeis=DataBaseService.getInstance(this)
        val db=databeis.writableDatabase
        if(db!=null){
            databeis.onUpgrade(db,3,4)
            var json:String?=null
            try {
                var a:Boolean=false
                val inputStream:InputStream=assets.open("countries_states_cities.json")
                json=inputStream.bufferedReader().use { it.readText() }
                var jsonarr=JSONArray(json)
                for (i in 0..jsonarr.length()-1){
                    a=false
                    var jsonobj=jsonarr.getJSONObject(i)
                    if(jsonobj.optJSONObject("translations")!=null){
                        if(jsonobj.optJSONObject("translations").optString("es")!=null){
                            a=true
                            println("("+(i+1)+", 0, 0, '"+jsonobj.optJSONObject("translations").optString("es")+"'),")
                            db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 0, 0,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")
                        }
                    } else {
                        println("("+(i+1)+", 0, 0, '"+jsonobj.getString("name")+"),")
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 0, 0,"+'"'+jsonobj.getString("name")+'"'+")")
                    }
                    if(jsonobj.optJSONArray("states")!=null){
                        var jsonarr2=jsonobj.getJSONArray("states")
                        for(j in 0..jsonarr2.length()-1){
                            var jsonobj2=jsonarr2.getJSONObject(j)
                            println("("+(i+1)+", "+(j+1)+", 0, '"+jsonobj2.getString("name")+"')")
                            db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", 0,"+'"'+jsonobj2.getString("name")+'"'+")")
                            if(jsonobj2.optJSONArray("cities")!=null){
                                var jsonarr3=jsonobj2.getJSONArray("cities")
                                for(k in 0..jsonarr3.length()-1){
                                    var jsonobj3=jsonarr3.getJSONObject(k)
                                    println("("+(i+1)+", "+(j+1)+", "+(k+1)+", '"+jsonobj3.getString("name")+"')")
                                    if(i==183&&j==34&&k==216){
                                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", "+(k+1)+", '"+jsonobj3.getString("name")+"')")
                                    } else {
                                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", "+(k+1)+", "+'"'+jsonobj3.getString("name")+'"'+")")
                                    }

                                }
                            } else {
                                db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", "+(j+1)+", 1,"+'"'+jsonobj2.getString("name")+'"'+")")
                            }
                        }
                    } else { if (a==true) {
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 0,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 1,"+'"'+jsonobj.optJSONObject("translations").optString("es")+'"'+")")
                    }
                        else {
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 0,"+'"'+jsonobj.getString("name")+'"'+")")
                        db.execSQL("INSERT INTO TABLE_UBIGEO (CODE_DEPARTAMENTO,CODE_PROVINCIA,CODE_DISTRITO,NOMBRE) VALUES ("+(i+1)+", 1, 1,"+'"'+jsonobj.getString("name")+'"'+")")
                        }
                    }
                }
            }catch (e:IOException){
                e.printStackTrace()
            }
        }
    }*/

}