package com.itsigned.huqariq.serviceclient

import android.util.Log
import com.google.gson.JsonObject
import com.itsigned.huqariq.model.*
import com.itsigned.huqariq.util.HttpRequestConstants.Companion.URL_BASE
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

const val TIME_OUT_MINUTE=3

/**
 * clase con la interface del Retrofit
 */
interface RafiService {

    @POST("/country")
    fun country(@Header("Content-Type")  contentType:String): Observable<Any>
    /*no pasa parametro, recepciona*/

    @POST("/state")
    fun state(@Header("Content-Type")  contentType:String, @Body body: sendCountry): Observable<Any>
    /*enviar parametro, countries_id*/

    @POST("/city")
    fun city(@Header("Content-Type")  contentType:String, @Body body: sendState): Observable<Any>
    /*enviar parametro, states_id*/
    /*cuando cargue, poner como primer opcion, seleccionar pais, estado, datos*/
    /*cuando selecciona */
    /**/

    @POST("/account")
    fun register(@Header("Content-Type")  contentType:String, @Body body: RegisterUserDto): Observable<RegisterUserDto>

    @POST("/login")
    fun loginApp(@Header("Content-Type")  contentType:String, @Body body: LoginRequestDto): Observable<LoginUserDto>

    @POST("/login_email")
    fun emailApp(@Header("Content-Type")  contentType:String, @Body body: LoginRequestDtoOnlyEmail): Observable<LoginUserDto>

    @POST("/code")
    fun code(@Header("Content-Type")  contentType:String, @Body body: RecuperateEmail): Observable<RecuperateEmail>
/*solo envia correo, envia el codigo al correo electronico, cuando se ha enviado */

    @POST("/password")
    fun password(@Header("Content-Type")  contentType:String, @Body body: SetNewPassEmail): Observable<SetNewPassEmail>
/*correo,codigo,nueva contraseña*/

    @Multipart
    @POST("/huqariq_audio")
    fun uploadAudio(@Part file: MultipartBody.Part): Observable<Any>
    /*envio es igual, formato nombre de audio*/
    /*correo electronico;fecha;id_lenguaje-int(chanca,collao,aimara);nroAudio*/

    @POST("/lang")
    fun getLanguage(@Header("Content-Type")  contentType:String): Observable<ResponseLangDto>

    @GET("/prompt_audio/{user_id}")
    fun downloadAudio(
            @Header("Cookie") session: String,
            @Path(value = "user_id")  userId:String
    ): Observable<ResponseBody>

    @POST("/check_email")
    fun verifyMail(@Body body: RequestValidateMail): Observable<ResponseBody>

    @POST("/dni")
    fun validateDni(@Body body: RequestValidateDni): Observable<ResponseBody>

    @POST("/dialecto_region")
    fun validateDialectByRegion(@Body body: FormDialectRegion): Observable<ResponseDialectRegion>

    @POST("/dialect")
    fun validateAnswerDialecto(@Body body: FormDialectAnswer): Observable<ResponseDialectAnswer>


    companion object Factory {
        fun create(): RafiService {
            Log.d("URL BASE", URL_BASE)
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL_BASE)
                    .build()
            return retrofit.create(RafiService::class.java)
        }

        fun createForFile(): RafiService {
            val client = OkHttpClient.Builder()
                    .connectTimeout(TIME_OUT_MINUTE.toLong(), TimeUnit.MINUTES).
                    writeTimeout(TIME_OUT_MINUTE.toLong(), TimeUnit.MINUTES)
                    .readTimeout(TIME_OUT_MINUTE.toLong(), TimeUnit.MINUTES).build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL_BASE)
                    .client(client)
                    .build()
            return retrofit.create(RafiService::class.java)
        }

    }
}