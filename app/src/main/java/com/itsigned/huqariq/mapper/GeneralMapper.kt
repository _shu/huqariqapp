package com.itsigned.huqariq.mapper

import com.itsigned.huqariq.bean.User
import com.itsigned.huqariq.model.LoginUserDto
import com.itsigned.huqariq.model.RegisterUserDto

class GeneralMapper {
    companion object{

        fun userToRegisterUserDto(toMap: User): RegisterUserDto {
            return RegisterUserDto(
                email = toMap.email,
                password = toMap.password,
                /*dni = toMap.dni,*/
                name = toMap.name,
                /*last_name = toMap.lastName,*/
                countries_id = toMap.codePais.toString(),
                cities_id = toMap.codeCiudad.toString(),
                states_id = toMap.codeEstado.toString(),
                /*phone = toMap.phone.toString(),*/
                native_lang=toMap.idLanguage
            )
        }

        fun loginUserDtoDtoToUser(toMap: LoginUserDto,actualLang: Int/*, contra:String*/): User {
            val user= User()
            /*user.password= contra*/
            user.email = toMap.email
            user.userExternId = toMap.user_id.toLong()
            /*user.dni = toMap.dni.toString()
            user.lastName = toMap.last_name*/
            user.name = toMap.name
            user.idLanguage=toMap.lang
            user.avance = toMap.count
            user.codePais = 1
            user.codeCiudad = 1
            user.codeEstado = 1
            user.lenguas = toMap.langs
            user.lengActual= actualLang
            return user
        }
    }
}