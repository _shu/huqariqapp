package com.itsigned.huqariq.bean;


import org.json.JSONArray;
import org.json.JSONObject;

public class User {

    private Object avance;
    private Object lenguas;

    private Integer lengActual;

    private Long userLocalId;
    private Long userExternId;
    /*private String dni;*/
    private String email;
    /*private String lastName;*/
    private String name;
    /*private String Phone;*/
    private Integer codePais;
    private Integer codeEstado;
    private Integer codeCiudad;
    private String password;
    private Integer idLanguage;

    private String institution;
    private String region;
    private Integer isMember;

    public Object getAvance() { return avance; }

    public void setAvance(Object avance) { this.avance = avance; }

    public Object getLenguas() { return lenguas; }

    public void setLenguas(Object lenguas) { this.lenguas = lenguas; }

    public Integer getLengActual() { return lengActual; }

    public void setLengActual(Integer lengActual) { this.lengActual = lengActual; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Long getUserLocalId() { return userLocalId; }

    public void setUserLocalId(Long userLocalId) { this.userLocalId = userLocalId; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    /*public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }*/

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    /*public String getPhone() { return Phone; }

    public void setPhone(String phone) { Phone = phone; }*/

    public Integer getCodePais() {
        return codePais;
    }

    public void setCodePais(Integer codePais) { this.codePais = codePais; }

    public Integer getCodeCiudad() {
        return codeCiudad;
    }

    public void setCodeCiudad(Integer codeCiudad) {
        this.codeCiudad = codeCiudad;
    }

    public Integer getCodeEstado() {
        return codeEstado;
    }

    public void setCodeEstado(Integer codeEstado) {
        this.codeEstado = codeEstado;
    }

    public Long getUserExternId() {
        return userExternId;
    }

    public void setUserExternId(Long userExternId) {
        this.userExternId = userExternId;
    }

    /*public String getDni() { return dni; }

    public void setDni(String dni) { this.dni = dni; }*/

    public String getInstitution() { return institution; }

    public void setInstitution(String institution) { this.institution = institution; }

    public String getRegion() { return region; }

    public void setRegion(String region) { this.region = region; }

    public Integer getIsMember() { return isMember; }

    public void setIsMember(Integer isMember) { this.isMember = isMember; }

    public Integer getIdLanguage() { return idLanguage; }

    public void setIdLanguage(Integer idLanguage) { this.idLanguage = idLanguage; }

}