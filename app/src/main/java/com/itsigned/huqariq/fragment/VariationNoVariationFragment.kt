package com.itsigned.huqariq.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import kotlinx.android.synthetic.main.android13.*

class VariationNoVariationFragment: Fragment() {

    var action:RegisterActivityTourInterface?=null
    var action2:MainActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android13, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else if (context is MainActivityTourInterface) {
            action2 = context as MainActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    override fun onDestroyView() {
        if (context is RegisterActivityTourInterface){
            action!!.changeBackgroundColor(R.drawable.degradado_status_bar)
        } else if (context is MainActivityTourInterface){
            action2!!.changeBackgroundColor(R.color.main_status_bar,R.color.white)
        }
        super.onDestroyView()
    }

    override fun onDetach() {
        if (context is RegisterActivityTourInterface){
            action!!.changeBackgroundColor(R.drawable.degradado_status_bar)
        } else if (context is MainActivityTourInterface){
            action2!!.uncheck(2,true)
        }
        super.onDetach()
    }

    private fun configureActionButton() {
        btnNextNoVar.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
    }

}