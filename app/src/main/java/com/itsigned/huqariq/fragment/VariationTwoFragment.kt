package com.itsigned.huqariq.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import kotlinx.android.synthetic.main.android12.*

class VariationTwoFragment: Fragment() {

    var action:RegisterActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android12, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }

    }

    private fun configureActionButton() {
        textView5.setOnClickListener {
            val intent: Intent = Intent(Intent.ACTION_SENDTO)
            val uriText="mailto:rjzevallos.salazar@gmail.com"+"?subject="+ Uri.encode("Acerca de Huqariq")+"&body="+ Uri.encode("Acerca de la aplicación Huqariq")
            val uri: Uri = Uri.parse(uriText)
            intent.setData(uri)
            startActivity(Intent.createChooser(intent,"Enviar correo"))
        }
        btnNextTwo.setOnClickListener {
            if (verifyRadioChecked()){
                val fragment=VariationThreeFragment.newInstance(0)
                action!!.customFragment(fragment,"ThirdVariationFragment","default")
            } else {
                action!!.changeBackgroundColor(R.drawable.degradado_construccion)
                action!!.customFragment(VariationNoVariationFragment(),"NoVariationFragment","default")
            }
        }
        butAchuar.setOnClickListener{uncheckOthers(0)}
        butAimara.setOnClickListener{uncheckOthers(1)}
        butAshaninka.setOnClickListener{uncheckOthers(2)}
        butAwajun.setOnClickListener{uncheckOthers(3)}
        butEseEja.setOnClickListener{uncheckOthers(4)}
        butHarakbut.setOnClickListener{uncheckOthers(5)}
        butKatakaibo.setOnClickListener{uncheckOthers(6)}
        butMatsigenka.setOnClickListener{uncheckOthers(7)}
        butQuechua.setOnClickListener{uncheckOthers(8)}
        butShawi.setOnClickListener{uncheckOthers(9)}
        butShipibo.setOnClickListener{uncheckOthers(10)}
        butYine.setOnClickListener{uncheckOthers(11)}
    }

    private fun uncheckOthers(dontUncheck:Int){
        buttonList(dontUncheck,true)
        for(i in 0 .. 11){
            if(i!=dontUncheck){
                buttonList(i,false)
            }
        }
    }

    private fun buttonList(index:Int,state:Boolean){
        when (index){
            0->{/*butAchuar.isEnabled=!state*/
                butAchuar.isChecked=state}
            1->{/*butAimara.isEnabled=!state*/
                butAimara.isChecked=state}
            2->{/*butAshaninka.isEnabled=!state*/
                butAshaninka.isChecked=state}
            3->{/*butAwajun.isEnabled=!state*/
                butAwajun.isChecked=state}
            4->{/*butEseEja.isEnabled=!state*/
                butEseEja.isChecked=state}
            5->{/*butHarakbut.isEnabled=!state*/
                butHarakbut.isChecked=state}
            6->{/*butKatakaibo.isEnabled=!state*/
                butKatakaibo.isChecked=state}
            7->{/*butMatsigenka.isEnabled=!state*/
                butMatsigenka.isChecked=state}
            8->{/*butQuechua.isEnabled=!state*/
                butQuechua.isChecked=state}
            9->{/*butShawi.isEnabled=!state*/
                butShawi.isChecked=state}
            10->{/*butShipibo.isEnabled=!state*/
                butShipibo.isChecked=state}
            11->{/*butYine.isEnabled=!state*/
                butYine.isChecked=state}
        }
    }


    private fun verifyRadioChecked():Boolean{
        when{
            butQuechua.isChecked==true -> return true
        }
        return false
    }

}