package com.itsigned.huqariq.fragment

/*import com.itsigned.huqariq.activity.RegisterActivityInterface*/

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import com.itsigned.huqariq.helper.*
import com.itsigned.huqariq.model.FormRegisterUserStepOneDto
import com.itsigned.huqariq.model.RequestValidateMail
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import kotlinx.android.synthetic.main.android3.*
import java.util.*
import kotlin.collections.ArrayList


const val STATUS_MAIL_NOT_CALL_SERVICE=0
const val STATUS_MAIL_GREEN=1
const val STATUS_MAIL_RED=2
const val STATUS_MAIL_LOADING=3



class RegisterFragment : Fragment() {

    private val GOOGLE_SIGN_IN=99
    private val callbackManager=CallbackManager.Factory.create()

    var action:RegisterActivityTourInterface?=null

    private var statusMail: Int=0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.android3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        statusMail=STATUS_MAIL_NOT_CALL_SERVICE
        mailInputText.onFocusChangeListener= View.OnFocusChangeListener {
            _, hasFocus -> if(!hasFocus){
                validMail(mailInputText.text.toString())
                /*ViewHelper.showOneView(checkMailImageView,validMailStatusFrame)
                statusMail=STATUS_MAIL_GREEN*/
                }
        }
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==GOOGLE_SIGN_IN){
            val task:com.google.android.gms.tasks.Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                if (account != null) {
                    /*if (!ValidationHelper.validateMail(account.email!!)) {
                        showMessage(getString(R.string.view_invalid_form_mail))
                        return
                    }*/
                    RafiServiceWrapper.verifyMail(context!!, RequestValidateMail(account.email!!),
                        { success ->
                            if (success.toUpperCase(Locale.ROOT)=="OK"){
                                val form = FormRegisterUserStepOneDto(account.email!!, "z18d5G5xcFg54"/*, "00000000"*/, account.displayName!!/*, ""*/)
                                action!!.setDataFormSteperOne(form)
                                val fragment = IntroOneFragment.newInstance(0)
                                println(form)
                                action!!.customFragment(fragment, "Slider0", "default")
                            }else {
                                showMessage(getString(R.string.view_form_register_step_one_message_mail_in_use))
                            }
                        },
                        {
                            val googleConf:GoogleSignInOptions =
                                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                    .requestIdToken(getString(R.string.default_web_client_id))
                                    .requestEmail()
                                    .build()
                            val googleClient:GoogleSignInClient=GoogleSignIn.getClient(activity!!,googleConf)
                            googleClient.signOut()
                            statusMail= STATUS_MAIL_NOT_CALL_SERVICE
                            showMessage(getString(R.string.default_error_server))
                        }
                    )
                }
            } catch (e: ApiException) {
                e.printStackTrace()
            }
        }
    }

    private fun configureActionButton() {
        showPassImageView.setOnClickListener{
            passInputText.transformationMethod=HideReturnsTransformationMethod.getInstance()
            ViewHelper.showOneView(hidePassImageView,showPassFrame)
        }
        hidePassImageView.setOnClickListener{
            passInputText.transformationMethod=PasswordTransformationMethod.getInstance()
            ViewHelper.showOneView(showPassImageView,showPassFrame)
        }
        showPassVerifyImageView.setOnClickListener{
            passInputVerifyText.transformationMethod=HideReturnsTransformationMethod.getInstance()
            ViewHelper.showOneView(hidePassVerifyImageView,showPassVerifyFrame)
        }
        hidePassVerifyImageView.setOnClickListener{
            passInputVerifyText.transformationMethod=PasswordTransformationMethod.getInstance()
            ViewHelper.showOneView(showPassVerifyImageView,showPassVerifyFrame)
        }
        btnCreaCuenta.setOnClickListener {
            val form=getForm()
            action!!.setDataFormSteperOne(form)
            val validForm=validateStepOneRegister(form,statusMail)
            if(validForm) {
                val fragment=IntroOneFragment.newInstance(0)
                action!!.customFragment(fragment,"Slider0","default")
            }
        }
        textoCondicionesCuenta.setOnClickListener {
            action!!.customFragment(UsageConditionsFragment(),"conditionsFragment","default")
        }
        LayoutGoogleButtonLinear.setOnClickListener {
            val googleConf:GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()
            val googleClient:GoogleSignInClient=GoogleSignIn.getClient(activity!!,googleConf)
            googleClient.signOut()
            startActivityForResult(googleClient.signInIntent,GOOGLE_SIGN_IN)
        }
        /*LayoutFacebookButtonLinear.setOnClickListener{
            LoginManager.getInstance().loginBehavior = LoginBehavior.WEB_ONLY
            var permission:ArrayList<String> = ArrayList()
            val list= arrayListOf<String>()
            list.add("email")
            list.add("public_profile")
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile","email"))
            LoginManager.getInstance().registerCallback(callbackManager,
                    object: FacebookCallback<LoginResult> {
                        override fun onSuccess(result: LoginResult?) {
                            val request=GraphRequest.newMeRequest(result?.accessToken){`object`, response ->
                                try {
                                    println(`object`.getString("name"))
                                    println(`object`)
                                    if (`object`.has("id")){
                                        /*if (!ValidationHelper.validateMail(`object`.getString("email"))) {
                                            showMessage(getString(R.string.view_invalid_form_mail))
                                        }*/
                                        RafiServiceWrapper.verifyMail(context!!, RequestValidateMail(`object`.getString("email")),
                                            { success ->
                                                if (success.toUpperCase(Locale.ROOT)=="OK"){
                                                    val form = FormRegisterUserStepOneDto(`object`.getString("email"), "z18d5G5xcFg54", "00000000", `object`.getString("name"), "")
                                                    action!!.setDataFormSteperOne(form)
                                                    println(form)
                                                    val fragment = IntroOneFragment.newInstance(0)
                                                    action!!.customFragment(fragment, "Slider0", "default")
                                                }else {
                                                    showMessage(getString(R.string.view_form_register_step_one_message_mail_in_use))
                                                }
                                            },
                                            {
                                                statusMail= STATUS_MAIL_NOT_CALL_SERVICE
                                                showMessage(getString(R.string.default_error_server))
                                            }
                                        )
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                            val parameters = Bundle()
                            parameters.putString("fields", "id,name,email")
                            request.parameters = parameters
                            request.executeAsync()

                        }

                        override fun onCancel() {
                            println("cancel")
                        }

                        override fun onError(error: FacebookException?) {
                            println("error")
                        }
                    }
            )
            LoginManager.getInstance().logOut()
        }*/
    }

    private fun getForm(): FormRegisterUserStepOneDto {
        return FormRegisterUserStepOneDto(
                mailInputText.text.toString(),
                passInputText.text.toString(),
                /*(9999999..10000000).random().toString(),*/
                nombreInputText.text.toString()/*,
                "" */)
    }

    fun validateStepOneRegister(formRegisterStepOne: FormRegisterUserStepOneDto,statusMail:Int):Boolean {

        when{
            statusMail!= STATUS_MAIL_GREEN->showMessage(context!!.getString(R.string.registro_message_error_correo_electronico_invalido))
            !ValidationHelper.validateMail(formRegisterStepOne.email)->showErrorMail()
            !ValidationHelper.validateStringEmpty(formRegisterStepOne.name)->showErrorName()
            !ValidationHelper.validatePassword(formRegisterStepOne.password)->showErrorPassword()
            formRegisterStepOne.password!=passInputVerifyText.text.toString()->showErrorRepeatPassword()
            !ValidationHelper.validateCheckBox(checkCondicionesUso.isChecked)->showErrorConditions()
            else->return true
        }
        return false
    }

    private fun validMail(mail:String){

        if (!ValidationHelper.validateMail(mail)) {
            ViewHelper.showOneView(errorMailImageView,validMailStatusFrame)
            statusMail= STATUS_MAIL_RED
            showMessage(getString(R.string.view_invalid_form_mail))
            return
        }
        statusMail= STATUS_MAIL_LOADING
        ViewHelper.showOneView(loadServiceMailProgress,validMailStatusFrame)
        RafiServiceWrapper.verifyMail(context!!, RequestValidateMail(mail),
                { success ->
                    evaluateVerificationServerMail(success)
                },
                {
                    statusMail= STATUS_MAIL_NOT_CALL_SERVICE
                    showMessage(getString(R.string.default_error_server))
                }
        )
    }

    private fun evaluateVerificationServerMail(verification: String) {
        statusMail = if (verification.toUpperCase(Locale.ROOT)=="OK"){
            ViewHelper.showOneView(checkMailImageView,validMailStatusFrame)
            STATUS_MAIL_GREEN
        }else {
            ViewHelper.showOneView(errorMailImageView,validMailStatusFrame)
            showMessage(getString(R.string.view_form_register_step_one_message_mail_in_use))
            STATUS_MAIL_RED
        }
    }

    private fun showErrorName() {setErrorEditText(nombreInputText,R.string.registro_message_error_ingrese_nombres)}

    private fun showErrorMail() {setErrorEditText(mailInputText,R.string.registro_message_error_ingrese_correo_electronico) }

    private fun showErrorPassword() {setErrorEditText(passInputText,R.string.registro_message_error_ingrese_contrasena_usuario) }

    private fun showErrorRepeatPassword() {setErrorEditText(passInputVerifyText,R.string.registro_message_error_repeat_usuario) }

    private fun showErrorConditions() {setErrorCheckBoxk(checkCondicionesUso,R.string.registro_message_error_acepte_condiciones) }

}