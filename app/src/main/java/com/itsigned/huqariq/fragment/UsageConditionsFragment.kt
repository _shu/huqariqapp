package com.itsigned.huqariq.fragment

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import androidx.core.app.NotificationCompat
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.MainActivity
import com.itsigned.huqariq.dialog.PlayAudioDialog
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.util.Util
import kotlinx.android.synthetic.main.android6.*
import java.io.File
import java.util.*

class UsageConditionsFragment : StepFragment() {

    private  var mediaPlayer: MediaPlayer? = null
    private var initRecord=false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android6, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onDestroyView() {
        mediaPlayer!!.stop()
        super.onDestroyView()
    }

    private fun configureActionButton() {
        regresarCondiciones.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
        descargarCondiciones.setOnClickListener{
            hackDisabled()
            val path= Util.saveConsentiment(context!!)
            sendNotification(path)
        }
        ttsPlayButton.setOnClickListener(){
            playAudio(R.raw.consentimientoaudio)
        }
        ttsPauseButton.setOnClickListener(){
            stopButton()
        }
    }

    private fun playAudio(audio: Int){
        mediaPlayer = MediaPlayer.create(context!!, audio)
        mediaPlayer!!.setOnCompletionListener { ViewHelper.showOneView(ttsPlayButton,conditionsSpeakerContainer) }
        mediaPlayer!!.start()
        initRecord=true
        ViewHelper.showOneView(ttsPauseButton,conditionsSpeakerContainer)
    }

    private fun stopButton() {
        mediaPlayer!!.stop()
        ViewHelper.showOneView(ttsPlayButton,conditionsSpeakerContainer)
    }

    private fun hackDisabled() {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun sendNotification(path: String) {
        Log.d("Send Notification","sendNotification")
        hackDisabled()
        val intent = Intent(context!!, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val pending = PendingIntent.getActivity(context!!,
                0, openFile(path),  PendingIntent.FLAG_ONE_SHOT)

        val notificationBuilder = NotificationCompat.Builder(context!!)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.myc)
                .setAutoCancel(false)
                .setContentTitle("consentimiento.pdf")
                .setContentText("Descargado")
                .addAction(R.mipmap.ic_launcher, "Abrir",
                        pending)
        //  .setAutoCancel(true)
        //  .setSound(defaultSoundUri)
        //   .setContentIntent(pendingIntent);
        val notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "Your_channel_id"
            val channel = NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
            notificationBuilder.setChannelId(channelId)
        }
        val `as` = notificationBuilder.build()
        `as`.flags = `as`.flags or Notification.FLAG_NO_CLEAR
        notificationManager.notify(10000, notificationBuilder.build())
    }

    private fun openFile(path:String):Intent{
        val file = File(path)
        val uri =  Uri.fromFile(file)
        val intent = Intent(Intent.ACTION_VIEW)
        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase(
                Locale.getDefault()))
        intent.setDataAndType(uri, mimeType)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        return  intent
    }

}