package com.itsigned.huqariq.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.GetFormDataStepperAction
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import com.itsigned.huqariq.bean.Ubigeo
import com.itsigned.huqariq.database.DataBaseService
import com.itsigned.huqariq.helper.getLoadingProgress
import com.itsigned.huqariq.helper.showMessage
import com.itsigned.huqariq.model.*
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import kotlinx.android.synthetic.main.android11.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

private const val SEARCH_COUNTRY=1
private const val SEARCH_CITY=2
private const val SEARCH_REGION=3

private var listaPais: ArrayList<Ubigeo>? = null
private var pais: String? = null
private var ciudad: String? = null
private var region: String? = null
private var idPais: String? = null
private var idCiudad: String? = null
private var idRegion: String? = null

private var ubigeoSelected:Ubigeo?=null

class VariationOneFragment: Fragment() {

    var action: RegisterActivityTourInterface?=null
    lateinit var customProgressDialog: Dialog


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.android11, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
        this.wrapperQueryDataBase(SEARCH_COUNTRY) { x->listaPais=x }
        configureSpinner()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        btnNext.setOnClickListener {
            val validForm=validateStepTwoRegister()
            val form=getForm()
            action!!.setDataFormSteperTwo(form)
            if (validForm){
                println(form)
                action!!.customFragment(VariationTwoFragment(),"SecondVariationFragment","default")
            }
        }
    }

    /**
     * Metodo para obtener los ubigeos de la base de datos interna
     * @param typeUbigeo=Tipo de ubigeo
     * @param idCountry=Id del pais a obtener
     * @param idCity= Id de la ciudad
     * @param getUbigeo lambda con donde se coloca la acción a realizar cuando se obtuvo exito en la consulta
     */
    private fun wrapperQueryDataBase(typeUbigeo:Int,idCountry:Int=0,idState:Int=0,getUbigeo: (listUbigeo:  ArrayList<Ubigeo>? ) -> Unit){
        var listUbigeo=ArrayList<Ubigeo>()
        when(typeUbigeo){
            SEARCH_COUNTRY -> /*dataBaseServiceTour.listDepartamento*/{
                val countryList=ArrayList<Ubigeo>()
                RafiServiceWrapper.getCountries(activity!!,
                    { jsonPaises ->
                        /*println(jsonPaises)*/
                        var listaPaisesString= jsonPaises.toString()
                        var paises:List<String> = listaPaisesString.split("=[{")
                        var pais=paises[1].split("}, ")
                        for (i in 0..pais.size-1){
                            /*println(pais[0])*/
                            var nombrePais=pais[i].split(".0, name=")
                            var codigoPais=nombrePais[0].split("=")
                            val ubigeo:Ubigeo=Ubigeo()
                            ubigeo.setIdDepartamento(codigoPais[1].toInt())
                            ubigeo.setIdProvincia(0)
                            ubigeo.setIdDistrito(0)
                            ubigeo.setNombre(nombrePais[1])
                            /*println(nombrePais[1])*/
                            countryList.add(ubigeo)
                        }
                        listaPais=countryList
                        val paisAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, listaPais!!.toMutableList())
                        spPais.adapter = paisAdapter
                    },{ error ->
                        Toast.makeText(activity!!, error, Toast.LENGTH_LONG).show()
                    })
                /*listUbigeo=countryList*/
                /*listaPais=countryList
                llenarPaisesEnDrop()*/
            }
            SEARCH_CITY -> /*dataBaseServiceTour.getListProvincia(idCountry)*/{
                val statesList=ArrayList<Ubigeo>()
                RafiServiceWrapper.getStates(activity!!, sendCountry(countries_id = idCountry),
                    { jsonEstados ->
                        println(jsonEstados)
                        var listaEstadosString= jsonEstados.toString()
                        var estados:List<String> = listaEstadosString.split("=[{")
                        var estado=estados[1].split("}, ")
                        for (i in 0..estado.size-1){
                            var nombreEstado=estado[i].split(".0, name=")
                            var codigoEstado=nombreEstado[0].split("=")
                            val ubigeo:Ubigeo=Ubigeo()
                            ubigeo.setIdDepartamento(idCountry)
                            ubigeo.setIdProvincia(codigoEstado[1].toInt())
                            ubigeo.setIdDistrito(0)
                            ubigeo.setNombre(nombreEstado[1])
                            println(nombreEstado[1])
                            statesList.add(ubigeo)
                        }
                        val ciudadAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, statesList)
                        spCiudad.adapter = ciudadAdapter
                    },{ error ->
                        Toast.makeText(activity!!, error, Toast.LENGTH_LONG).show()
                    })
                listUbigeo=statesList
            }

            SEARCH_REGION ->  /*dataBaseServiceTour.getListDistrito(idCountry, idCity)*/{
                val citiesList=ArrayList<Ubigeo>()
                RafiServiceWrapper.getCities(activity!!, sendState(countries_id = idCountry,states_id = idState),
                    { jsonCiudades ->
                        println(jsonCiudades)
                        if (!jsonCiudades.toString().equals("{city=[]}")) {
                            var listaCiudadesString= jsonCiudades.toString()
                            var ciudades:List<String> = listaCiudadesString.split("=[{")
                            var ciudad=ciudades[1].split("}, ")

                            for (i in 0..ciudad.size-1){
                                var nombreCiudad=ciudad[i].split(".0, name=")
                                var codigoCiudad=nombreCiudad[0].split("=")
                                val ubigeo:Ubigeo=Ubigeo()
                                ubigeo.setIdDepartamento(idCountry)
                                ubigeo.setIdProvincia(idState)
                                ubigeo.setIdDistrito(codigoCiudad[1].toInt())
                                ubigeo.setNombre(nombreCiudad[1])
                                println(nombreCiudad[1])
                                citiesList.add(ubigeo)
                            }

                        } else {
                            val ubigeo:Ubigeo= Ubigeo()
                            ubigeo.setIdDepartamento(idCountry)
                            ubigeo.setIdProvincia(idState)
                            ubigeo.setIdDistrito(idState)
                            ubigeo.setNombre(spCiudad.selectedItem.toString())
                            println(spCiudad.selectedItem.toString())
                            citiesList.add(ubigeo)
                        }
                        val regionAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, citiesList)
                        spRegion.adapter = regionAdapter
                    },{ error ->
                        Toast.makeText(activity!!, error, Toast.LENGTH_LONG).show()
                    })
                listUbigeo=citiesList
            }
            else->throw java.lang.Exception()
        }
        getUbigeo(listUbigeo)
    }

    /**
     * Método para llenar los spinner iniciales y configurar sus eventos
     */
    private fun configureSpinner(){
        spPais.onItemSelectedListener =   getListenerSpinner { position->spinnerItemSelectedPais(position) }
        spCiudad.onItemSelectedListener = getListenerSpinner { position->spinnerItemSelectCiudad(position) }
        spRegion.onItemSelectedListener = getListenerSpinner { position->spinnerItemSelectRegion(position) }
        /*val paisAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, listaPais!!.toMutableList())
        spPais.adapter = paisAdapter*/
    }

    private fun llenarPaisesEnDrop(){
        val paisAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, listaPais!!.toMutableList())
        spPais.adapter = paisAdapter
    }

    private fun getListenerSpinner(itemSelect: (position: Int) -> Unit): AdapterView.OnItemSelectedListener {
        return object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                itemSelect(position)
            }
        }
    }

    private fun spinnerItemSelectedPais(position: Int) {
        val ubigeo = spPais.getItemAtPosition(position) as Ubigeo
        pais = ubigeo.nombre
        idPais=ubigeo.idDepartamento.toString()
        var listaCiudad: ArrayList<Ubigeo> = ArrayList()
        wrapperQueryDataBase(SEARCH_CITY,ubigeo.idDepartamento){ x->  listaCiudad =x!! }
        val ciudadAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, listaCiudad)
        spCiudad.adapter = ciudadAdapter
    }

    /**
     * Metodo que indica la acción a realizar cuando se selecciona una ciudad
     * @param position posicion de la lista de la ciudad seleccionada
     */
    private fun spinnerItemSelectCiudad(position: Int) {
        val ubigeo = spCiudad.getItemAtPosition(position) as Ubigeo
        ciudad = ubigeo.nombre
        idCiudad=ubigeo.idProvincia.toString()
        var listaRegion: ArrayList<Ubigeo> = ArrayList()
        wrapperQueryDataBase(SEARCH_REGION,ubigeo.idDepartamento,ubigeo.idProvincia){ x->  listaRegion =x!! }
        val regionAdapter = ArrayAdapter(context!!, R.layout.support_simple_spinner_dropdown_item, listaRegion)
        spRegion.adapter = regionAdapter
    }

    /**
     * Metodo que indica la acción a realizar cuando se selecciona una region
     * @param position posicion de la lista de la ciudad seleccionada
     */
    private fun spinnerItemSelectRegion(position: Int) {
        val ubigeo = spRegion.getItemAtPosition(position) as Ubigeo
        region = pais + "/" + ciudad + "/" + ubigeo.nombre
        idRegion=ubigeo.idDistrito.toString()
        ubigeoSelected = ubigeo
        /*customProgressDialog=getLoadingProgress()
        customProgressDialog.show()*/
        /*RafiServiceWrapper.validateDialectByRegion(context!!, FormDialectRegion(pais!!, ciudad!!, ubigeo.nombre),
            { x->
                val quantity=if(x.dialecto=="0") 4 else 3
                /*action!!.changeQuantityTab(quantity)*/
                if(quantity==3) {
                    val idDialect=if (x.dialecto.toUpperCase(Locale.ROOT)=="CHANCA") 1
                    else ( if(x.dialecto.toUpperCase(Locale.ROOT) == "COLLAO") 2 else -1)
                    action!!.setDataFormSteperThree(FormRegisterStepThreeDto(idDialect.toString()))
                }
                println(x.dialecto)
                println("esta bien")
                customProgressDialog.dismiss()
            },{
                Log.i("debugging","aqui esta el error")
                Toast.makeText(context!!,R.string.default_error_server, Toast.LENGTH_LONG).show()
                customProgressDialog.dismiss()
            }
        )*/

        /*
        val quantity=if(isSouth())  3 else 4
        action!!.changeQuantityTab(quantity)

         */
    }

    private fun getForm(): FormRegisterUserStepTwoDto {
        return FormRegisterUserStepTwoDto(idPais!!, idRegion!!, idCiudad!!/*,""*/,1)
    }

    private fun validateStepTwoRegister():Boolean {
        when{
            pais==null->showMessage(context!!.getString(R.string.register_select_region))
            ciudad== null->showMessage(context!!.getString(R.string.register_select_province))
            region==null->showMessage(context!!.getString(R.string.register_select_district))
            else->return true
        }
        return false
    }

}

