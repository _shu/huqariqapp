package com.itsigned.huqariq.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.itsigned.huqariq.R
import com.itsigned.huqariq.helper.ViewHelper
import kotlinx.android.synthetic.main.android30.*

class DonateMethosFragment : Fragment() {

    var tipoMet:Int?=null

    companion object {
        fun newInstance(tipo: Int)=DonateMethosFragment().apply {
            arguments=Bundle().apply {
                putInt("tipo",tipo)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android30, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configurate()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        arguments?.getInt("tipo")?.let {
            tipoMet=it
        }
    }

    private fun configurate() {
        if (tipoMet==1){
            ViewHelper.showOneView(imgDonationPlin2,methodTypeLayout)
            ViewHelper.showOneView(imgPlin,methodImageLayout)
        } else if (tipoMet==2){
            ViewHelper.showOneView(imgDonationYape2,methodTypeLayout)
            ViewHelper.showOneView(imgYape,methodImageLayout)
        } else if (tipoMet==3){
            ViewHelper.showOneView(imgDonationPaypal2,methodTypeLayout)
            ViewHelper.showOneView(imgPaypal,methodImageLayout)
        }
        btnBackDon.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
    }

}