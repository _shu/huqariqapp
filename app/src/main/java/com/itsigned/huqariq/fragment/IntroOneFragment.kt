package com.itsigned.huqariq.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import com.itsigned.huqariq.activity.StartActivity
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_intro_tour.*
import kotlinx.android.synthetic.main.android7_10stepper.*
import java.util.*


class IntroOneFragment : Fragment() {

    var action: RegisterActivityTourInterface?=null
    /*private var actual:Int=0*/
    private var stepper:View?=null
    private var image:View?=null
    private var actual:Int?=null
    var texto= arrayListOf<String>()

    companion object {
        fun newInstance(int: Int)=IntroOneFragment().apply {
            arguments=Bundle().apply {
                putInt("index",int)
            }
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.activity_intro_tour, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        texto= arrayListOf(resources.getString(R.string.intro_image_one),resources.getString(R.string.intro_image_two),resources.getString(R.string.intro_image_three),resources.getString(R.string.intro_image_four))
        updateStepper(actual!!)
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        arguments?.getInt("index")?.let {
            actual=it
        }
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        introTourLayout.setOnClickListener {
            if(actual!!+1<4){
                val fragment=newInstance(actual!!+1)
                action!!.customFragment(fragment,"Slider"+(actual!!+1),"default")
            } else {
                action!!.customFragment(VariationOneFragment(),"VariationFragment","default")
            }
        }
    }

    private fun updateStepper(index:Int){
        val sdk = android.os.Build.VERSION.SDK_INT
        for (i in 0 .. 3){
            getSliderIndexNumber(i)
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                stepper!!.setBackgroundDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_intro_elipse_inactive) )
            } else {
                stepper!!.background = ContextCompat.getDrawable(activity!!, R.drawable.ic_intro_elipse_inactive)
            }
        }
        getSliderIndexNumber(index)
        ViewHelper.showOneView(image!!,introImageLayout)
        textIntro.text=texto[actual!!]
        /*if(actual==3){
            textEspere.visibility=View.VISIBLE
        }*/
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            stepper!!.setBackgroundDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_intro_elipse_active) )
        } else {
            stepper!!.background = ContextCompat.getDrawable(activity!!, R.drawable.ic_intro_elipse_active)
        }
    }

    private fun getSliderIndexNumber(position:Int){
        when(position){
            0 ->{stepper=introOne
                image=imageIntroOne}
            1 ->{stepper=introTwo
                image=imageIntroTwo}
            2 ->{stepper=introThree
                image=imageIntroThree}
            3 ->{stepper=introFour
                image=imageIntroFour}
            else ->throw Exception("out limit step")
        }
    }

}