package com.itsigned.huqariq.fragment

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.MainActivityTourInterface
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.LoginRequestDto
import com.itsigned.huqariq.model.LoginRequestDtoOnlyEmail
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.android24.*


class MainProgressFragment : Fragment() {

    var action: MainActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android24, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val textLengua = TextView(activity!!)
        val textAvance = TextView(activity!!)
        var listaLenguas:IntArray? = null
        var listaAvances:IntArray? = null
        println(SessionManager.getInstance(activity).userLogged.email.toString())
        RafiServiceWrapper.loginApp(activity!!, LoginRequestDtoOnlyEmail(SessionManager.getInstance(activity).userLogged.email.toString()),
            { loginUser ->
                println(loginUser)
                if(loginUser.langs.toString().equals("[]")){
                    textLengua.text=Html.fromHtml("<font color=#DCECFC> Quechua Chanca </font>")
                    progresosLenguasLayout.addView(textLengua)
                    textLengua.setTextSize(20F)
                    textAvance.text=Html.fromHtml("<font color=#DCECFC> 0 / 250 </font>")
                    progresosLenguasLayout.addView(textAvance)
                    textAvance.setTextSize(16F)
                } else {
                    val langsinin=loginUser.langs.toString().split("[")
                    val langssinfin=langsinin[1].split("]")
                    val idLenguas=langssinfin[0].split(",")

                    val avansinin=loginUser.count.toString().split("[")
                    val avanssinfin=avansinin[1].split("]")
                    val idAvance=avanssinfin[0].split(",")


                    for (i in 0 .. idLenguas.size-1){
                        println(idLenguas[i].split(".")[0])
                        if (idLenguas[i].split(".")[0].toInt()==1){
                            textLengua.text=Html.fromHtml("<font color=#DCECFC> Quechua Chanca </font>")
                        } else if (idLenguas[i].split(".")[0].toInt()==2){
                            textLengua.text=Html.fromHtml("<font color=#DCECFC> Quechua Collao </font>")
                        }
                        /*textLengua.text=Html.fromHtml("<font color=#DCECFC>"+idLenguas[i].split(".")[0]+" / 250 </font>")*/
                        progresosLenguasLayout.addView(textLengua)
                        textLengua.setTextSize(20F)
                        println(idAvance[i].split(".")[0])
                        textAvance.text=Html.fromHtml("<font color=#DCECFC>"+idAvance[i].split(".")[0]+" / 250 </font>")
                        progresosLenguasLayout.addView(textAvance)
                        textAvance.setTextSize(16F)
                    }
                }

                /*listaLenguas=loginUser.langs*/

                /*for (i in 0 .. idAvance.size-1){
                    println(idAvance[i].split(".")[0])
                }*/
                /*listaAvances=loginUser.count*/
                },
            { error -> print("error") })
        /*for (i in 0 .. listaLenguas!!.size) {
            textLengua.text=Html.fromHtml("<font color=#DCECFC>"+listaLenguas!![i].toString()+" / 250 </font>")
            progresosLenguasLayout.addView(textLengua)
            textLengua.setTextSize(18F)
            textAvance.text=Html.fromHtml("<font color=#DCECFC>"+listaAvances!![i].toString()+" / 250 </font>")
            progresosLenguasLayout.addView(textAvance)
            textAvance.setTextSize(16F)
        }*/

        /*when(SessionManager.getInstance(activity).userLogged.idLanguage){
            1->textLengua.text="Quechua chanca"
            2->textLengua.text="Quechua collao"
            null->textLengua.text="no tiene idioma"
        }
        textLengua.setTextSize(18F)
        textAvance.setTextSize(16F)
        /*textAvance.text=Html.fromHtml("<font color=#DCECFC>"+SessionManager.getInstance(activity).userLogged.password.toString()+" / 250 </font>")*/
        RafiServiceWrapper.loginApp(activity!!, LoginRequestDtoOnlyEmail(SessionManager.getInstance(activity).userLogged.email.toString()),
                { loginUser ->
                    val texto=if  (loginUser.count==null||loginUser.count.toString()=="")0 else  loginUser.count.toString().replace("[", "").replace("]", "").toFloat().toInt()
                    textAvance.text=Html.fromHtml("<font color=#DCECFC>"+texto+" / 250 </font>") },
                { error -> print("error") })
        progresosLenguasLayout.addView(textLengua)
        progresosLenguasLayout.addView(textAvance)*/
        funciones()
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityTourInterface) {
            action = context as MainActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
        action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_progreso)
    }

    override fun onDestroyView() {
        action!!.changeBackgroundColor(R.color.main_status_bar,R.color.white)
        super.onDestroyView()
    }

    override fun onDetach() {
        action!!.uncheck(1,true)
        super.onDetach()
    }

    private fun funciones(){
        btnBackProgress.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
    }

}