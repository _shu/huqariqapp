package com.itsigned.huqariq.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.LoginActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import com.itsigned.huqariq.dialog.PlayAudioDialog
import com.itsigned.huqariq.helper.*
import com.itsigned.huqariq.model.FormDialectAnswer
import com.itsigned.huqariq.model.FormRegisterStepThreeDto
import com.itsigned.huqariq.player.MediaPlayerHolder
import com.itsigned.huqariq.player.MediaRecordHolder
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.RecordConstants
import kotlinx.android.synthetic.main.android14.*
import kotlinx.android.synthetic.main.android14.view.*
import kotlinx.android.synthetic.main.android23.view.*
import kotlinx.android.synthetic.main.fragment_step_three.*
import java.lang.RuntimeException
import java.util.*

class VariationThreeFragment: Fragment(){

    private  var mediaPlayer: MediaPlayer? = null
    private var initRecord=false

    var action: RegisterActivityTourInterface?=null

    lateinit var customProgressDialog: Dialog

    private var actual:Int?=null

    companion object {
        fun newInstance(int: Int)=VariationThreeFragment().apply {
            arguments=Bundle().apply {
                putInt("index",int)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.android14, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
        val listQuestionNumber= arrayListOf(
                "Pregunta 1","Pregunta 2","Pregunte 3","Pregunta 4","Pregunta 5")
        val listOptionQuestion = arrayListOf(
                arrayOf("willka","alchhi","irqi","haway","warma"),
                arrayOf("urku","ñawi","mat’i","maki","uma"),
                arrayOf("yaku","t’uru","kachi","yawar","mitu"),
                arrayOf("qiwa","sacha","qhipa","paqu","q’achu"),
                arrayOf("qhilli","qanra","pichay","anka","hawa"))
        val listAudioQuestion= arrayListOf(R.raw.questionone,R.raw.questiontwo,R.raw.questionthree,R.raw.questionfour,R.raw.questionfive)
        fillRadioGroup(questionRadioGroup,listOptionQuestion[actual!!])
        buttonPreguntaUno.text=listQuestionNumber[actual!!]
        imageAltavozInactive.setOnClickListener{
            playAudio(listAudioQuestion[actual!!])
        }
        imageAltavozActive.setOnClickListener{
            stopButton()
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        arguments?.getInt("index").let {
            actual=it
        }
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    override fun onDestroyView() {
        if (initRecord==true){
            mediaPlayer!!.stop()
        }
        super.onDestroyView()
    }

    private fun configureActionButton() {
        btnNextThree.setOnClickListener {
            if(actual!!+1<5){
                action!!.addResponse(actual!!,convertCodeAnswerToString(questionRadioGroup.checkedRadioButtonId))
                val fragment=newInstance(actual!!+1)
                action!!.customFragment(fragment,"Question"+(actual!!+1)+"VariationFragment","default")
            } else {
                action!!.addResponse(actual!!,convertCodeAnswerToString(questionRadioGroup.checkedRadioButtonId))
                action!!.verifyDialect()
            }
        }
    }

    private fun playAudio(audio: Int){
        mediaPlayer = MediaPlayer.create(context!!, audio)
        mediaPlayer!!.setOnCompletionListener {
            ViewHelper.showOneView(imageAltavozInactive,imageQuestionFrame)
            initRecord=false}
        mediaPlayer!!.start()
        initRecord=true
        ViewHelper.showOneView(imageAltavozActive,imageQuestionFrame)
    }

    private fun stopButton() {
        mediaPlayer!!.stop()
        ViewHelper.showOneView(imageAltavozInactive,imageQuestionFrame)
    }


    private fun fillRadioGroup(radioGroup: RadioGroup, option:Array<String>){
        var index=0
        for (question in option) {
            val radioButton = RadioButton(context!!)
            radioButton.text=question
            radioButton.id=index
            index += 1
            radioGroup.addView(radioButton)
        }
        radioGroup.check(0)
    }

    private fun convertCodeAnswerToString(code:Int):String{
        return when(code){
            0->"a"
            1->"b"
            2->"c"
            3->"d"
            4->"e"
            else->""
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode== REQUEST_PERMISION_PLAY_AUDIO){

            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context,R.string.message_need_permission, Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(context,R.string.message_acept_permission, Toast.LENGTH_LONG).show()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}