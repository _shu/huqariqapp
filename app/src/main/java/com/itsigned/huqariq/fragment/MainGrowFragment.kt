package com.itsigned.huqariq.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.MainActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import kotlinx.android.synthetic.main.android29.*
import kotlinx.android.synthetic.main.android4.*

class MainGrowFragment : Fragment() {

    var action: MainActivityTourInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android29, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityTourInterface) {
            action = context as MainActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }

    }

    private fun configureActionButton() {
        imgDonationPaypal.setOnClickListener {
            val fragment=DonateMethosFragment.newInstance(3)
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, fragment)
            at.addToBackStack(null)
            at.commit()
        }
        imgDonationPatreon.setOnClickListener {
            val link:String="https://www.patreon.com/siminchikkunarayku?fan_landing=true"
            val intent:Intent=Intent(Intent.ACTION_VIEW, Uri.parse(link))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            /*intent.setPackage("com.android.chrome")*/
            try {
                context!!.startActivity(Intent.createChooser(intent,"Continuar"))
            } catch (e:Exception) {
                e.printStackTrace()
            }
        }
        imgDonationPlin.setOnClickListener {
            val fragment=DonateMethosFragment.newInstance(1)
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, fragment)
            at.addToBackStack(null)
            at.commit()
        }
        imgDonationYape.setOnClickListener {
            val fragment=DonateMethosFragment.newInstance(2)
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, fragment)
            at.addToBackStack(null)
            at.commit()
        }
        btnBackDon.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
        /*imgDonationPaypal.setOnClickListener{
            action!!.paypalPayment()
        }*/
    }
}