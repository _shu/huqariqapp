package com.itsigned.huqariq.fragment

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.MainActivityTourInterface
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.android21.*

class MainStartFragment : Fragment() {

    var action: MainActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android21, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        textoBienvenida.text= Html.fromHtml("Hola <font color=#3D80CF>"+SessionManager.getInstance(activity).userLogged.name+"</font>")
        if(SessionManager.getInstance(activity).userLogged.idLanguage==1){
            textInicioLenguaVariacion.text="Quechua chanca"
            ViewHelper.showOneView(imgQuechuaChanca,imgLanguageIcon)
        } else if (SessionManager.getInstance(activity).userLogged.idLanguage==2) {
            textInicioLenguaVariacion.text="Quechua collao"
            ViewHelper.showOneView(imgQuechuaCollao,imgLanguageIcon)
        }
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityTourInterface) {
            action = context as MainActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        constraintGrabarFrasesConstraint.setOnClickListener {
            action!!.uncheck(1,false)
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, MainPhrasesFragment())
            at.addToBackStack("default")
            at.commit()
        }

        constraintVerProgresoConstraint.setOnClickListener {
            action!!.uncheck(1,false)
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutMainTour, MainProgressFragment())
            at.addToBackStack("default")
            at.commit()
        }
    }

}