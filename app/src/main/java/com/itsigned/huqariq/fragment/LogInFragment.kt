package com.itsigned.huqariq.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.LoginActivityTourInterface
import com.itsigned.huqariq.helper.goToActivity
import com.itsigned.huqariq.helper.hasErrorEditTextEmpty
import com.itsigned.huqariq.helper.showError
import com.itsigned.huqariq.mapper.GeneralMapper
import com.itsigned.huqariq.model.FormRegisterUserStepOneDto
import com.itsigned.huqariq.model.LoginRequestDto
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.itsigned.huqariq.util.Util
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.RuntimeException


class LogInFragment : Fragment() {

    private val GOOGLE_LOG_IN=98
    private val callbackManager= CallbackManager.Factory.create()

    var action: LoginActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is LoginActivityTourInterface) {
            action = context as LoginActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==GOOGLE_LOG_IN){
            val task:com.google.android.gms.tasks.Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                if (account != null) {
                    action!!.login_google_facebook(account.email!!)
                } else {
                    Toast.makeText(context!!,"Usuario no encontrado, porfavor, registrese",Toast.LENGTH_LONG)
                }
            } catch (e: ApiException) {
                e.printStackTrace()
            }
        }
    }

    private fun configureActionButton() {
        LayoutGoogleButtonLogin.setOnClickListener {
            val googleConf: GoogleSignInOptions =
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestIdToken(getString(R.string.default_web_client_id))
                            .requestEmail()
                            .build()
            val googleClient: GoogleSignInClient = GoogleSignIn.getClient(activity!!,googleConf)
            /*googleClient.signOut()*/
            startActivityForResult(googleClient.signInIntent,GOOGLE_LOG_IN)
        }
        /*LayoutFacebookButtonLogin.setOnClickListener{
            LoginManager.getInstance().loginBehavior = LoginBehavior.WEB_ONLY
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("email","public_profile"))
            LoginManager.getInstance().registerCallback(callbackManager,
                    object: FacebookCallback<LoginResult> {
                        override fun onSuccess(result: LoginResult?) {
                            val request= GraphRequest.newMeRequest(result?.accessToken){ `object`, response ->
                                try {
                                    println(`object`.getString("name"))
                                    println(`object`)
                                    if (`object`.has("id")){
                                        action!!.login_google_facebook(`object`.getString("email"))
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                            val parameters=Bundle()
                            parameters.putString("fields","name,email")
                            request.parameters=parameters
                            request.executeAsync()
                            /*println("correcto")
                            result?.let {
                                val accessToken:String=it.accessToken.token
                                println(accessToken)
                                println("esto funcionando? ptmr")
                            }*/
                        }

                        override fun onCancel() {
                            println("cancel")
                        }

                        override fun onError(error: FacebookException?) {
                            println("error")
                        }
                    }
            )
            LoginManager.getInstance().logOut()
        }*/
        btnLogin.setOnClickListener {
            action!!.login()
        }
        tv_forget_password.setOnClickListener {
            val at: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
            at.replace(R.id.layoutLoginTour, NewPassAccFragment())
            at.addToBackStack(null)
            at.commit()
        }
    }


}