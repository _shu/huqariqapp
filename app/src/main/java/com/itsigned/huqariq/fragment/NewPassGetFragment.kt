package com.itsigned.huqariq.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.*
import com.itsigned.huqariq.helper.ValidationHelper
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.helper.setErrorEditText
import com.itsigned.huqariq.helper.showMessage
import com.itsigned.huqariq.model.*
import com.itsigned.huqariq.serviceclient.RafiServiceWrapper
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.android27.*
import java.lang.RuntimeException
import java.util.*


class NewPassGetFragment : StepFragment() {

    var action: LoginActivityTourInterface?=null

    private var mail:String?=null

    companion object {
        fun newInstance(correo: String)=NewPassGetFragment().apply {
            arguments=Bundle().apply {
                putString("correo",correo)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android27, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        arguments?.getString("correo")?.let {
            mail=it
        }
        if (context is LoginActivityTourInterface) {
            action = context as LoginActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        showPasswordImageView.setOnClickListener{
            etPassword.transformationMethod= HideReturnsTransformationMethod.getInstance()
            ViewHelper.showOneView(hidePasswordImageView,showPasswordFrame)
        }
        hidePasswordImageView.setOnClickListener{
            etPassword.transformationMethod= PasswordTransformationMethod.getInstance()
            ViewHelper.showOneView(showPasswordImageView,showPasswordFrame)
        }
        showPasswordVerifyImageView.setOnClickListener{
            etVerification.transformationMethod= HideReturnsTransformationMethod.getInstance()
            ViewHelper.showOneView(hidePasswordVerifyImageView,showPasswordVerifyFrame)
        }
        hidePasswordVerifyImageView.setOnClickListener{
            etVerification.transformationMethod= PasswordTransformationMethod.getInstance()
            ViewHelper.showOneView(showPasswordVerifyImageView,showPasswordVerifyFrame)
        }
        /*btnCreaCuenta.setOnClickListener {
            val intent = Intent(this, RegisterActivityn::class.java)
            startActivity(intent)
        }*/
        btnNewPassGenerate.setOnClickListener {
            println(mail)
            if (ValidationHelper.validateStringEmpty(etCodigo.text.toString())){
                if (ValidationHelper.validateStringEmpty(etPassword.text.toString())){
                    if (ValidationHelper.validateStringEmpty(etVerification.text.toString())){
                        if (etPassword.text.toString()==etVerification.text.toString()){
                            action!!.setNewPass(mail=mail!!,code=etCodigo.text.toString(),pass=etPassword.text.toString())
                            activity!!.supportFragmentManager!!.popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                            /*RafiServiceWrapper.setNewPass(activity!!, SetNewPassEmail(mail!!,etCodigo.text.toString(),etPassword.text.toString()),
                                { activity?.supportFragmentManager!!.popBackStack(0,FragmentManager.POP_BACK_STACK_INCLUSIVE) },
                                { error ->
                                    Toast.makeText(activity!!, error, Toast.LENGTH_LONG).show() })*/
                        } else {
                            setErrorEditText(etVerification,R.string.registro_message_error_repeat_usuario)
                        }
                    } else {
                        setErrorEditText(etVerification,R.string.registro_message_error_ingrese_contrasena_usuario)
                    }
                } else {
                    setErrorEditText(etPassword,R.string.registro_message_error_ingrese_contrasena_usuario)
                }
            } else {
                setErrorEditText(etCodigo,R.string.np_code_vacio)
            }

        }
        btnBackNewPass2.setOnClickListener {
            activity?.supportFragmentManager!!.popBackStack()
        }
    }
}