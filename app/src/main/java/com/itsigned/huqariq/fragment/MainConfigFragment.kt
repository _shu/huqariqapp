package com.itsigned.huqariq.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.IntroActivityTourInterface
import com.itsigned.huqariq.activity.MainActivityTourInterface
import com.itsigned.huqariq.helper.ViewHelper
import com.itsigned.huqariq.util.session.SessionManager
import kotlinx.android.synthetic.main.android25.*
import kotlinx.android.synthetic.main.android25.butAchuar
import kotlinx.android.synthetic.main.android25.butAimara
import kotlinx.android.synthetic.main.android25.butAshaninka
import kotlinx.android.synthetic.main.android25.butAwajun
import kotlinx.android.synthetic.main.android25.butEseEja
import kotlinx.android.synthetic.main.android25.butHarakbut
import kotlinx.android.synthetic.main.android25.butKatakaibo
import kotlinx.android.synthetic.main.android25.butMatsigenka
import kotlinx.android.synthetic.main.android25.butQuechua
import kotlinx.android.synthetic.main.android25.butShawi
import kotlinx.android.synthetic.main.android25.butShipibo
import kotlinx.android.synthetic.main.android25.butYine

class MainConfigFragment : Fragment() {

    var action: MainActivityTourInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android25, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityTourInterface) {
            action = context as MainActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        btnCloseSession.setOnClickListener {
            showDialogCloseSession()
            val googleConf: GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()
            val googleClient: GoogleSignInClient = GoogleSignIn.getClient(activity!!,googleConf)
            googleClient.signOut()
        }
        textView5.setOnClickListener {
            val intent:Intent=Intent(Intent.ACTION_SENDTO)
            val uriText="mailto:rjzevallos.salazar@gmail.com"+"?subject="+Uri.encode("Acerca de Huqariq")+"&body="+Uri.encode("Acerca de la aplicación Huqariq")
            val uri:Uri=Uri.parse(uriText)
            intent.setData(uri)
            startActivity(Intent.createChooser(intent,"Enviar correo"))
            /*startActivity(Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:rjzevallos.salazar@gmail.com")))*/
        }
        butAchuar.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butAchuar.isChecked=false }
        butAimara.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butAimara.isChecked=false}
        butAshaninka.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butAshaninka.isChecked=false}
        butAwajun.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butAwajun.isChecked=false}
        butEseEja.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butEseEja.isChecked=false}
        butHarakbut.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butHarakbut.isChecked=false}
        butKatakaibo.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butKatakaibo.isChecked=false}
        butMatsigenka.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butMatsigenka.isChecked=false}
        butQuechua.isEnabled=false
        butShawi.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butShawi.isChecked=false}
        butShipibo.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butShipibo.isChecked=false}
        butYine.setOnClickListener{
            action!!.changeBackgroundColor(android.R.color.transparent,R.drawable.degradado_grabacion)
            action!!.uncheck(2,false)
            action!!.changeFragment(VariationNoVariationFragment())
            butYine.isChecked=false}
    }

    /**
     * Método que muestra un dialogo para cerrar la sesión
     */
    private fun showDialogCloseSession(){

        val builder1 = AlertDialog.Builder(activity!!)
        builder1.setMessage("¿Desea cerrar la sesión?")
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Si"
        ) { dialog, id ->
            activity!!.finish()
            SessionManager.getInstance(activity!!).logoutApp()
            dialog.cancel() }

        builder1.setNegativeButton(
                "No"
        ) { dialog, id -> dialog.cancel() }

        val alert11 = builder1.create()
        alert11.show()
    }

    /*override fun onDestroyView() {
        action!!.changeBackgroundColor(R.color.main_status_bar,R.color.white)
        super.onDestroyView()
    }*/

}