package com.itsigned.huqariq.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.itsigned.huqariq.R
import com.itsigned.huqariq.activity.MainActivityTour
import com.itsigned.huqariq.activity.RegisterActivityTourInterface
import com.itsigned.huqariq.helper.ViewHelper
import kotlinx.android.synthetic.main.android19.*
import java.lang.RuntimeException

class VariationResultFragment: Fragment() {

    var action: RegisterActivityTourInterface?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.android19, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureActionButton()
        if(action!!.sayDialect()==1){
            textResultadoVariedad.text="Quechua Chanca"
            ViewHelper.showOneView(imageIconoLenguaChanca,variedadLenguaFrame)
        } else if (action!!.sayDialect()==2){
            textResultadoVariedad.text="Quechua Collao"
            ViewHelper.showOneView(imageIconoLenguaCollao,variedadLenguaFrame)
        } else {
            textResultadoVariedad.text="Error"
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is RegisterActivityTourInterface) {
            action = context as RegisterActivityTourInterface
        } else {
            throw RuntimeException (context!!.toString())
        }
    }

    private fun configureActionButton() {
        buttonEmpezar.setOnClickListener {
            action!!.registerNewUser()
        }
    }

}